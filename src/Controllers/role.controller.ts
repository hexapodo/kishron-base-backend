import { Request, Response } from 'express'
import { getRepository} from 'typeorm';
import  { Role } from '../entity/Role';

export const getRoles = async(req: Request, res: Response): Promise<Response> => {
    const roles = await getRepository(Role).find();
    roles.forEach(async (role) => {
        console.log('users', await role.users);
    });
    return res.json(roles);
}

export const getRole = async(req: Request, res: Response): Promise<Response> => {
    const role = await getRepository(Role).findOne(req.params.id);
    if (role) {
        return res.json(role);
    }
    return res.status(404).json({msg: 'role not found'});
}

export const createRole = async(req: Request, res: Response): Promise<Response> => {
    const newRole = getRepository(Role).create(req.body);
    const results = await getRepository(Role).save(newRole);
    return res.json(results);
}

export const modifyRole = async(req: Request, res: Response): Promise<Response> => {
    const role = await getRepository(Role).findOne(req.params.id);
    if (role) {
        console.log('modifyRole');
        getRepository(Role).merge(role, req.body);
        const results = await getRepository(Role).save(role);
        return res.json(results);
    }
    return res.status(404).json({msg: 'role not found'});
}

export const deleteRole = async(req: Request, res: Response): Promise<Response> => {
    const result = await getRepository(Role).delete(req.params.id);
    return res.json(result);
}
