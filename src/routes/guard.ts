import { config } from '../config';
import { verify } from 'jsonwebtoken';
import { Request, Response } from 'express'


export const guard = (req: Request, res: Response, next: any) => {
    const token: string = req.headers[config.auth.tokenKey] as string;
    if (token) {
        verify(token, config.auth.secret, {}, (err, decoded) => {
            if (err) {
                res.statusCode = 403;
                return res.json({ mensaje: config.auth.messages.noValid });
            }
            next();
        });
    } else {
        res.statusCode = 500;
        res.send({
            mensaje: config.auth.messages.required
        });
    }
};