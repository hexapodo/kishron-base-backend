import { Request, Response } from 'express'
import { getRepository} from 'typeorm';
import  { Photo } from '../entity/Photo';

export const getPhotos = async(req: Request, res: Response): Promise<Response> => {
    const photos = await getRepository(Photo).find();
    return res.json(photos);
}

export const getPhoto = async(req: Request, res: Response): Promise<Response> => {
    const photo = await getRepository(Photo).findOne(req.params.id);
    if (photo) {
        console.log(photo.user);
        return res.json(photo);
    }
    return res.status(404).json({msg: 'photo not found'});
}

export const createPhoto = async(req: Request, res: Response): Promise<Response> => {
    const newPhoto = getRepository(Photo).create(req.body);
    const results = await getRepository(Photo).save(newPhoto);
    return res.json(results);
}

export const modifyPhoto = async(req: Request, res: Response): Promise<Response> => {
    const photo = await getRepository(Photo).findOne(req.params.id);
    if (photo) {
        console.log('modifyPhoto');
        getRepository(Photo).merge(photo, req.body);
        const results = await getRepository(Photo).save(photo);
        return res.json(results);
    }
    return res.status(404).json({msg: 'photo not found'});
}

export const deletePhoto = async(req: Request, res: Response): Promise<Response> => {
    const result = await getRepository(Photo).delete(req.params.id);
    return res.json(result);
}
