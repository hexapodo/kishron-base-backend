import { Router } from 'express';
const router = Router();

import { getPhoto, createPhoto, getPhotos, modifyPhoto, deletePhoto } from '../Controllers/photo.controller'

router.get('/photos', getPhotos);
router.get('/photos/:id', getPhoto);
router.post('/photos', createPhoto);
router.put('/photos/:id', modifyPhoto);
router.delete('/photos/:id', deletePhoto);

export default router;