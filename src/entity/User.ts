import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Photo } from './Photo';
import { Role } from './Role';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    login: string;

    @Column()
    password: string;

    @Column()
    salt: string;

    @OneToMany( type => Photo, photo => photo.user)
    photos: Photo[];

    @ManyToMany(type => Role, role => role.users)
    @JoinTable()
    roles: Role[];
}