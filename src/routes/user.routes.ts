import { Router } from 'express';
import { UserController } from '../Controllers/user.controller'
import { guard } from './guard';

const router = Router();

router.get('/users', guard, UserController.getUsers);
router.get('/users/:id', UserController.getUser);
router.post('/users', guard, UserController.createUser);
router.put('/users/:id', guard, UserController.modifyUser);
router.delete('/users/:id', guard, UserController.deleteUser);

export default router;